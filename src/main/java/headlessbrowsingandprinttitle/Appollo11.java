package headlessbrowsingandprinttitle;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;

public class Appollo11 {

    WebDriver driver;
    public void webdriverinit(){

        System.setProperty("webdriver.chrome.driver", "src/chromedriver_win32/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--window-size=1400,800");
        driver = new ChromeDriver(options);

    }
    public void openindiatoday() {

        driver.get("https://www.indiatoday.in/");
    }

    public ArrayList fetchsportsnews() {

        ArrayList<String> sportslist=new ArrayList<String>();
        sportslist.add(driver.findElement(By.xpath("//*[@id=\"card_1206533_itg-block-7\"]/p[1]/a")).getText());
        sportslist.add(driver.findElement(By.xpath("//*[@id=\"card_1206533_itg-block-7\"]/p[2]/a")).getText());
        return sportslist;
    }

    public void closedriver() {

        driver.quit();
    }

}
